# NOTE: This is not working, since tensorflow/keras work only with python 3.7 and
# this project relies on Python 3.8.

from imageai.Prediction import ImagePrediction
import os

execution_path = os.getcwd()

prediction = ImagePrediction()

prediction.setModelTypeAsSqueezeNet()
prediction.setModelPath(os.path.join(execution_path, "squeezenet_weights_tf_dim_ordering_tf_kernels"))
prediction.loadModel()

predictions, probabilities = prediction.predictImage(os.path.join(execution_path,'images', "giraffe.jpg"), result_count=5 )
for eachPrediction, eachProbability in zip(predictions, probabilities):
    print(eachPrediction , " : " , eachProbability)